#include <QDebug>
#include <QtDBus/QtDBus>
#include <QSignalSpy>

#ifdef _USE_GPG
#include <QGpgME/Protocol>
#include <QGpgME/VerifyDetachedJob>
#include <QGpgME/ImportJob>

#include <gpgme++/verificationresult.h>
#include <gpgme++/importresult.h>
#endif

#include "udisky.h"
#include "ui_udisky.h"
#include "usbdevice.h"

#include <fcntl.h>
#include <unistd.h>

typedef QHash<QString, QVariant> Properties;
typedef QHash<QString, Properties> InterfacesAndProperties;
typedef QHash<QDBusObjectPath, InterfacesAndProperties> DBusIntrospection;
Q_DECLARE_METATYPE(Properties)
Q_DECLARE_METATYPE(InterfacesAndProperties)
Q_DECLARE_METATYPE(DBusIntrospection)

udisky::udisky(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::udisky)
{
    //m_filePath = "/home/jr/src/iso/neon-unstable-20230212-1117.iso";
    //QString m_filePath = "/home/jr/tmp/sig/hello";
    m_filePath = "/home/jr/src/iso/neon-unstable-20230212-1117.iso";
    //QString signingKeyFile = "/usr/share/isoimagewriter/neon-signing-key.gpg";
    //QString signingKeyFile = "/home/jr/tmp/sig/jriddell.gpg";
    QString signingKeyFile = "/home/jr/tmp/sig/neon.gpg";
    //QString signingKeyFile = "/home/jr/tmp/sig/neon2.gpg";

    m_ui->setupUi(this);

    qDebug() << "XX udisky init";
//    QTimer::singleShot(2000, this, &udisky::writeDisk);
    QTimer::singleShot(2000, this, &udisky::verifyIso);
}

void udisky::verifyIso()
{
    QFileInfo fileInfo2(m_filePath);
    QString fileName = fileInfo2.fileName();
    QString keyFingerprint;
    importSigningKey("neon-signing-key.gpg", keyFingerprint);
    qDebug() << "keyFingerprint" << keyFingerprint;
    QString sigFilePath = m_filePath + ".sig";
    QFileInfo fileInfo(sigFilePath);
    QString sigFileName = fileInfo.fileName();
    if (!QFile::exists(sigFilePath)) {
        qDebug() << "Could not find %1, please download PGP signature file ";
    }

    auto signatureFile = std::shared_ptr<QIODevice>(new QFile(sigFilePath));
    qDebug() << "sigFilePath" << sigFilePath;
    if (!signatureFile->open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open signature file";
        return;
    }

    auto isoFile = std::shared_ptr<QIODevice>(new QFile(m_filePath));
    if (!isoFile->open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open ISO image";
        return;
    }

#ifdef _USE_GPG
    QGpgME::VerifyDetachedJob *job = QGpgME::openpgp()->verifyDetachedJob();
    connect(job, &QGpgME::VerifyDetachedJob::result, this, [this](GpgME::VerificationResult result)
    {
        qDebug() <<  "VerifyDetachedJob result() signal";
        GpgME::Signature signature = result.signature(0);
        qDebug() << "summary: " << signature.summary();
        this->summaryResult = signature.summary();
        Q_EMIT asyncDone();
    });
    job->start(signatureFile, isoFile);
    QSignalSpy spy(this, SIGNAL(asyncDone()));
    spy.wait(20000);

    qDebug() << "this->summaryResult" << this->summaryResult;
    if (summaryResult & GpgME::Signature::Valid) {
        qDebug() << "GpgME::Signature::Valid;";
        //m_isIsoValid = VerifyResult::Successful;
    } else if (summaryResult & GpgME::Signature::KeyRevoked) {
        qDebug() << "GpgME::Signature::KeyRevoked";
        //m_error = i18n("Key is revoked.");
        //m_isIsoValid = VerifyResult::Failed;
    } else if (summaryResult & GpgME::Signature::Red) {
        qDebug() << "GpgME::Signature::Red";
    } else {
        qDebug() << "Uses wrong signature." << summaryResult;
        //m_error = i18n("Uses wrong signature.");
        //m_isIsoValid = VerifyResult::Failed;
    }
#else
        qDebug() << "This app is built without verification support.";
        qDebug() << "VerifyResult::KeyNotFound";
#endif

    qDebug() << "emit finished(m_isIsoValid, m_error);";
}

bool udisky::importSigningKey(const QString &fileName, QString &keyFingerprint)
{
    //QString signingKeyFile = "/usr/share/isoimagewriter/neon-signing-key.gpg";
    //QString signingKeyFile = "/home/jr/tmp/sig/jriddell.gpg";
    //QString signingKeyFile = "/home/jr/tmp/sig/neon.gpg";
    QString signingKeyFile = "/home/jr/tmp/sig/neon2.gpg";
    if (signingKeyFile.isEmpty()) {
        qDebug() << "error can't find signing key" << signingKeyFile;
        return false;
    }

    QFile signingKey(signingKeyFile);
    if (!signingKey.open(QIODevice::ReadOnly)) {
        qDebug() << "error" << signingKey.errorString();
        return false;
    }

    QByteArray signingKeyData = signingKey.readAll();

#ifdef _USE_GPG
    QGpgME::ImportJob *importJob = QGpgME::openpgp()->importJob();
    GpgME::ImportResult importResult = importJob->exec(signingKeyData);

    if (!(importResult.numConsidered() == 1
          && (importResult.numImported() == 1
              || importResult.numUnchanged() == 1))) {
        qDebug() << "Could not import gpg signature";
        return false;
    }

    keyFingerprint = QString(importResult.import(0).fingerprint());

    return true;
#endif
    return false;
}

void udisky::writeDisk()
{
    qDebug() << "XX udisky writeDisk() start";
    QFile imageFile;
    imageFile.setFileName("/home/jr/Downloads/dsl-4.4.10.iso");

    QIODevice* device;
    device = &imageFile;
    device->open(QIODevice::ReadOnly);
    qDebug() << "XX udisky writeDisk() got input";

    QDBusInterface deviceDBus("org.freedesktop.UDisks2", "/org/freedesktop/UDisks2/block_devices/sda", "org.freedesktop.UDisks2.Block", QDBusConnection::systemBus(), this);
    //QDBusReply<QDBusUnixFileDescriptor> reply = deviceDBus.call(QDBus::Block, "OpenDevice", "rw", Properties{{"flags", O_DIRECT | O_SYNC | O_CLOEXEC}} );
    QDBusReply<QDBusUnixFileDescriptor> reply = deviceDBus.call(QDBus::Block, "OpenDevice", "rw", Properties{{"flags", O_SYNC | O_CLOEXEC}} );
    QDBusUnixFileDescriptor fd = reply.value();
    QFile deviceFile;
    bool isFileOpen = deviceFile.open(fd.fileDescriptor(), QIODevice::WriteOnly);
    qDebug() << "XX udisky writeDisk() got output and opened file: " << isFileOpen;
    qDebug() << "XX udisky writeDisk() got output and opened file: " << isFileOpen;

    const qint64 TRANSFER_BLOCK_SIZE = 1024 * 1024;
    void* buffer = NULL;
    buffer = malloc(TRANSFER_BLOCK_SIZE);
    if (buffer == NULL) {
        throw "Failed to allocate memory for buffer.";
    }
    qint64 readBytes;
    qint64 writtenBytes;
    // Start reading/writing cycle
    for (;;)
    {
        if ((readBytes = device->read(static_cast<char*>(buffer), TRANSFER_BLOCK_SIZE)) <= 0) {
            qDebug() << "XX udisky writeDisk() input device read() was <= 0: " << readBytes;
            break;
        }
        qDebug() << "XX udisky writeDisk() input device read() cycle, was " << readBytes;

        readBytes = alignNumber(readBytes, (qint64)512);
        writtenBytes = deviceFile.write(static_cast<char*>(buffer), readBytes);
        //writtenBytes = ::write(fd.fileDescriptor(), buffer, readBytes);
        qDebug() << "write writtenBytes: " << writtenBytes;
        if (writtenBytes < 0) {
            qDebug() << "write writtenBytes: " << writtenBytes;
            //throw i18n("Failed to write to the device:\n%1"); //, "ook"); //deviceFile.errorString());
        }

    }

    qDebug() << "XX udisky writeDisk() end";

}
udisky::~udisky() = default;
