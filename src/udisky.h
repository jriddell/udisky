#ifndef UDISKY_H
#define UDISKY_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class udisky;
}

class udisky : public QMainWindow
{
    Q_OBJECT

public:
    explicit udisky(QWidget *parent = nullptr);
    ~udisky() override;
    bool importSigningKey(const QString &fileName, QString &keyFingerprint);
Q_SIGNALS:
    void asyncDone();
public Q_SLOTS:
    void writeDisk();
    void verifyIso();
private:
    QScopedPointer<Ui::udisky> m_ui;
    QString m_filePath;
    QString signingKeyFile;
    int summaryResult;
};

#endif // UDISKY_H
